---
title: Agriculture
description: 
published: true
date: 2021-12-31T14:24:24.504Z
tags: 
editor: markdown
dateCreated: 2021-11-16T15:54:40.189Z
---

# Welcome to the Agriculture Home Page

- [Permaculture *Top page of our Permaculture section*](/agriculture/permaculture)
- [Guerrilla Gardening *Top page for our Guerilla Gardening section*](/agriculture/guerilla-gardening)
{.links-list}


## Books
- [ ] Clean this section up

*Botany For Gardeners* by Brian Capon as a great entry-level resource on the subject

*The Soil Will Save Us* by Kristin Ohlson (quick and uplifting read, covers soil carbon sequestration and a bit of regenerative agriculture/grazing)

*Dirt: The Erosion of Civilizations* by David R. Montgomery (long, a bit dry in parts, not for everyone. Mainly a history book focusing on soil/erosion/agriculture through the ages)

*Trees of Power* by Akiva Silver (covers ten trees in detail and their uses, and their potential as perennial cash crops. It's not all fruits, folks!)

*The Omnivore's Dilemma* by Michael Pollan (long but very interesting read about four very different methods of food production. One of them is regenerative agriculture!)