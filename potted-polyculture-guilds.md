---
title: Potted Polyculture Guilds
description: 
published: false
date: 2022-06-22T16:29:37.221Z
tags: 
editor: markdown
dateCreated: 2022-05-23T19:11:42.085Z
---

# Potted Polyculture Guilds
In the microcosm of a pot applying the principle of having:
1. canopy layer, keystone tree species
1. vine/aerial layer
1. shrub small tree layer
1. rhizosphere/bulbs layer
1. ground covering herbaceous layer

one thing to look for are inserting nitrogen fixing plants in the various "non main" layers
such as a vine(peas) or groundcover(clover) 

as well as observing what complementing needs your primary plant
such as planting onion, and basil with your kale to repel aphids.

https://www.youtube.com/watch?v=j0fIdF05Pds