---
title: Applying your Bokashi Pre-Compost to the Soil
description: 
published: false
date: 2022-06-21T14:20:12.010Z
tags: compost, home-gardening, bokashi
editor: markdown
dateCreated: 2022-06-21T14:20:12.010Z
---

# Applying your Bokashi to the soil

"In the ground:  Once your precompost has been transferred to your garden, two weeks is usually all it takes for the majority of the items to be assimilated into the soil web.  However, if the temperature is cooler, or the food scraps are not fully precomposted, it might need longer.  An extra week in the soil should finish it off."

"Outdoors: Basically the same as above but outside. Protect from rain. You can also use a raised bed or corner of a non-weedy garden bed to make soil instead of planting. Dig down your bokashi buckets in the same spot so you have a constant supply of good soil. When you need ready bokashi soil for a project, it’s just to fill a bucket or a wheelbarrow with what you need."


Sources:
1. https://bokashiliving.com/troubleshooting-what-to-do-if-you-bokashi-bin-goes-bad/
1. https://bokashiworld.blog/learn/faq/
1. https://www.youtube.com/watch?v=0k3PTUnDHSI