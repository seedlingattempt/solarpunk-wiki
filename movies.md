---
title: Solarpunk Movies
description: Fancy watching something different from all the climate apocalypses? Here is the place where you can find all the stories in all the beautiful possible futures ahead, and in all the languages!
published: true
date: 2022-01-04T21:00:25.507Z
tags: 
editor: markdown
dateCreated: 2022-01-04T20:19:12.327Z
---

# Solarpunk Movies
## Animation
- 🇯🇵 **Nausicaä of the Valley of the Wind**, Hayao Miyazaki (Japan 1984)
- 🇯🇵 **Laputa: Castle in the Sky**, Hayao Miyazaki (Japan 1986)
- 🇦🇺 **FernGully: The Last Rainforest**, Bill Kroyer (1992)
- 🇯🇵 **Pom Poko**, Isao Takahata (Japan 1994)
- 🇯🇵 **Princess Mononoke**, Hayao Miyazaki (Japan 1997)
- 🇺🇸 **Treasure Planet**, Ron Clements & John Musker (USA 2002)
- 🇺🇸 **WALL-E**, Andrew Stanton (USA 2008)

## Live Action
- 🇫🇷 **The Year 01**, Jacques Doillon (France 1973)
- 🇺🇸 **Beasts of the Southern Wild**, Benh Zeitlin (USA 2012) 
- 🇺🇸 **Black Panther**, Ryan Coogler (USA 2018) 
- 🇬🇧/🇳🇬 **The Boy Who Harnessed The Wind**, Chiwetel Ejiofor (United Kingdom 2019)
- 🇰🇷 **Okja**, Bong Joon Ho (South Korea 2017)
