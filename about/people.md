---
title: The People Responsible
description: Find out about the people who are working on the content and maintenance of this site
published: true
date: 2022-01-04T20:49:32.220Z
tags: 
editor: markdown
dateCreated: 2021-11-17T09:11:12.146Z
---

# The People Responsible

This is a short introduction of the people who are actively working on and maintaining this site and who themselves decide to be added to this 

## Seedling Attempt - Administrator / Hardware Maintainer

I am a web developer and philosophy enthusiast who has decided that something has to give and that my best course of action for contributing to the problems in society is through tech praxis and critique.

You can find me on Mastodon [@seedlingattempt@kolektiva.social](https://kolektiva.social/web/accounts/106663937110988667) and on [YouTube](https://www.youtube.com/channel/UCmcVD_A9vDptpnvvA1x_k4A).

## Clockwork - Editor (Fiction/Books)

[He/him] Theoretical physicist, aspiring writer and lifelong learner and dreamer, I decided to step out of my scientific background to help creating stories towards a better future. I also watch too much esports. 

You can find me on [Mastodon](https://mastodon.uno/web/@clockwork), [Noblogo](https://noblogo.org/clockwork/) and Discord (Clockwork#0794)