---
title: Contributing
description: Information on how to contribute to this wiki
published: true
date: 2021-11-17T13:06:29.383Z
tags: 
editor: markdown
dateCreated: 2021-11-17T09:52:35.771Z
---

# How to contribute
We're happy that you're interested in contributing to this project! We hope that we can build a collective of knowledgeable individuals that can each add their own special skills to the information gathered here.

## Getting a user account

At moment of writing (see timestamp of edit) we do not have an emailing service setup attached to this domain. This means that at the moment we have a bit of a hard time utilizing the normal patterns of email verification that is commonly used to allow people self-service in registering.

Our current system of aquiring a user account and editing rights requires knowing an administrator and getting them to create a useraccount for you, see [The People Responsible](/about/people) for contact information to administrators. If interest grows and this system proves cumbersome, we are going to be working towards implementing a better registration system.

## Guides to WikiJS

- [Base Editing *Basic instruction for how to use WikiJS*](https://docs.requarks.io/guide/intro)
- [Navigation Overview *If you want to know what is possible to do within the WikiJS framework then you can find that here*](https://docs.requarks.io/en/navigation)
{.links-list}

## Content guidelines

We need some.

## Governance

This needs to be worked on for the future.

> As a rule of thumb I want to build systems that make despotism and heirarchy the least valuable proposition for any given individual with power. My personal approach to this project is thus - following that statement - that any guide or piece of information gathered on this site *ought* to be subject not only to consensus rule by the affected parties, be they editors or readers, but also that it should be possible for a dissenting minority to at any point fork this project and continue on their own path. The worst situation I can imagine for a project like this one is that schisms happen for whatever reason and the project dies simply because of a sudden lack of momentum. It is much preferable for *anyone* to copy what is in existence *now* in order to steer that into a different direction if and when they feel that such a split is warranted, without the possibility for the hypothetical dominant side to make those actions harder.
>
> This is why I want to emphasize the importance of spreading the information structure that we're gathering here to as many places as possible and the tools to influence that structure into as many minds and hands as possible.
> - Seedling Attempt
