---
title: Polyculture
description: 
published: true
date: 2022-06-17T13:23:12.088Z
tags: polyculture, companion planting, intercropping, permaculture
editor: markdown
dateCreated: 2022-06-17T13:22:26.645Z
---

# Polyculture
This concept is also referred to as companion planting, intercropping.

In agriculture, this is the practice of growing more than one plant species in close proximity with each other. The same way one would find different plant species growing together in nature. This is the opposite of the practice of monoculture in modern farms, wherein a single plant species is grown in an area.

Polyculture can be done for the mutual benefit of all the participating plants or for the benefit of one plant. 
<br>
*"A familiar example of companion planting is the Three Sisters trio—maize, climbing beans, and winter squash—which were commonly planted together by various Native American communities due to the plants’ complementary natures: the tall corn supports climbing beans, the low-growing squash shades the ground to prevent moisture loss and its big, prickly leaves discourage weeds and pests; and the fast-growing beans are ‘nitrogen fixers’ which make nitrogen available to other plants."* 

Quoted from Almanac: What is Companion Planting?^1^
<br>

Sources:
1. https://www.almanac.com/companion-planting-guide-vegetables