---
title: Bokashi Composting, Why do it?
description: What are the unique benefits of Bokashi Composting?
published: true
date: 2022-06-22T16:23:02.363Z
tags: compost, home-gardening, bokashi
editor: markdown
dateCreated: 2022-06-18T07:49:51.233Z
---

# Why do Bokashi Composting?

1. Bokashi is unique in being able to compost fats, meats, cheeses, cooked leftovers, and kitchen scraps in general
2. Efficiently composts your food scraps (quicker to decompose in soil because of the pre fermentation being done)
3. Increasing carbon content/humus of the soil, promoting soil life. Works similarly to adding soil amendments.
4. Helps fights plant disease because of some of its anti-pathogenic properties.

Sources:
1. Epic Gardening.(2019) *Bokashi Composting from Start to Finish (DIY Bokashi Bucket)*. Youtube. 
https://www.youtube.com/watch?v=0k3PTUnDHSI&t=491s 