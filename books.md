---
title: Solarpunk Books
description: Fancy reading some non-theory? Here is the place where you can find all the stories in all the beautiful possible futures ahead, and in all the languages!
published: true
date: 2021-12-01T11:38:27.586Z
tags: 
editor: markdown
dateCreated: 2021-11-19T17:39:28.637Z
---

# Solarpunk Fiction Library
Here is a list of the most notable titles of Solarpunk literature. Something is missing? Contact us!

## Solarpunk Precursors 

* 🇺🇸 **Dispossessed: an ambiguos utopia**, Ursula K. LeGuin (USA 1974)
* 🇺🇸 **Ecotopia: The Notebooks and Reports of William Weston**, Ernest Callenbach (USA 1975)
* 🇺🇸 **Pacific Edge**, Kim Stanley Robinson (USA 1990)
* 🇺🇸 **The Fifth Sacred Thing**, Miriam "Starhawk" Simos (USA 1993)

## Solarpunk Proper

* 🇺🇸 **2312**, Kim Stanley Robinson (USA 2012)
* 🇧🇷 **Solarpunk: Histórias ecológicas e fantásticas em um mundo sustentável**, Gerson Lodi-Ribeiro (Brazil 2012)
* 🇧🇷 **The Summer Prince**, Alaya Dawn Johnson (USA/Brazil 2013)
* 🇺🇸 **Suncatcher**, Alia Gee (USA 2014)
* 🇺🇸 **Biketopia: Feminist Bicycle Science Fiction Stories in Extreme Futures**, Elly Blue (USA 2017)
* 🇺🇸 **New York 2140**, Kim Stanley Robinson (USA 2017)
* 🇺🇸 **Sunvault: Stories of Solarpunk and Eco-Speculation**, Phoebe Wagner & Brontë Christopher Wieland (USA 2017)
* 🇺🇸 **Wings of Renewal: A Solarpunk Dragon Anthology**, Claudie Arseneault & Brenda J. Pierson (USA 2017)
* 🇺🇸 **Glass and Gardens: Solarpunk Summers**, Sarena Ulibarri (USA 2018)
* 🇺🇸 **Glass and Gardens: Solarpunk Winters**, Sarena Ulibarri (USA 2020)
* 🇺🇸 **The Weight of Light: A Collection of Solar Futures**, Joey Eschrich & Clark A. Miller (USA 2019)
* 🇮🇹 **Solarpunk: Come ho imparato ad amare il futuro**, Fabio Fernandes & Francesco Verso (Italy 2020)
* 🇮🇹 **Assalto al sole. La prima antologia solarpunk di autori italiani**, Franco Ricciardiello (Italy 2020)
* 🇺🇸 **A Psalm for the Wild-Built**, Becky Chambers (USA 2021)
* 🇫🇮 **Aurinkosydän: Kaikuja Paremmista Tulevaisuuksista**, Camilla Kantola, Teemu Korpijärvi & Reetta Vuokko-Syrjänen (Finland 2021)
* 🇳🇿 **Foxhunt**, Rem Wigmore (New Zealand 2021)
* 🇺🇸 **A Prayer for the Crown-Shy**, Becky Chambers (USA 2022)