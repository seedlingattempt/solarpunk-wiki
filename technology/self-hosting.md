---
title: Self-hosting Home Page
description: Top level page for self-hosting in the 21st century. All you need to get up and running with your own services for friends and family. Learn how to set up reliable services which serve your needs to low costs.
published: true
date: 2022-01-04T16:05:45.858Z
tags: intro, self-hosting
editor: markdown
dateCreated: 2021-11-16T15:25:23.886Z
---

# Welcome to the self-hosting top level page

Hi and welcome to what we hope is a comprehensive guide to self-hosting on the modern web. There's a lot to cover here but and we intend to take it one step at a time.

## Overview

Here follows an overview of the topics that we cover and an index for the entire self-hosting parts of this wiki. We hope that by reading and understanding this section you can get more of a sense for what can be learned here and how everything fits together.

- [Hardware Overview](/technology/self-hosting/hardware)
- [Operating Systems Overview](/technology/self-hosting/operating-systems)
- [Networking Overview](/technology/self-hosting/networking)
{.links-list}

## Who is these pages for, really?

We're writing these guides and compiling tactics for self-hosting with a pretty specific persona in mind. Maybe you're the person in your family or affinity-group that always helps out with technology problems, or maybe you just have a thing for tech and want to learn how to do things with some existing hardware for practically no cost

What is important is that you shouldn't *need* to know programming to understand the majority of the methods of self-hosting that we're going to be describing here.

## What can you do with this?

Here follows a list of the most popular services that fit very neatly into a self-hosting environment, regardless of performance levels of the hardware that you have access to. The perfect service for a low-performance and low-maintenance self-hosting environment consists of something that sees low average use, with few to no peaks in traffic. 

What this means is that the ideal service that you can provide for yourself and those near you is something which you and a couple of people can share and use intermittently without requiring massive amounts of computational data. This is also the same type of web service that the current cloud providers are really bad at providing, seeing as the lowest-cost server instance will still be more costly than the old computer you already own.

### Services {.tabset}

#### Mastodon
Mastodon is a micro-blogging software closely resembling Twitter. The difference is that Federation (and specifically [ActivityPub](https://en.wikipedia.org/wiki/ActivityPub)) is integral to the Mastodon project, which means that it is a perfect candidate for self-hosting. The reason for this is that when federation is a big part of any project, it becomes feasible to run instances intended to be used for as little as one single person. So it doesn't matter if you are the only person on your self-hosted instance, as long as you can federate with bigger instances and join the community that way.

The self-hosting also means that it will be much easier for you or your affinity group to control access to your data, which is a big plus. Of course, any publicly available post will potentially be scraped, so there should be no illusion that this is an enormous step up from Twitters ability to harvest your data.

However, one of the things that many users like about Mastodon is the fact that it is free from algorithms. This means that the system will only ever show you posts from people that you follow.

The Federated structure also removes the single-actor capitalization schemes based on user behavior data that is the primary revenue streams of companies like Facebook.

- [Mastodon Join Page](https://joinmastodon.org/)
- [Mastodon Github](https://github.com/mastodon/mastodon)
- [Mastodon Install Documentation](https://docs.joinmastodon.org/admin/prerequisites/)
{.links-list}

#### PixelFed

#### Bookwyrm

#### PeerTube

#### Matrix

#### Gitea

## A quick summary on Federated software

## Why are we doing this?

The justifications for a project like this is actually quite simple: We feel that a resource like this just doesn't exist. 

What more is that the philosophical underpinnings of other self-hosting proponents doesn't seem very geared towards either Mutual Aid\[insert link\] or the Solarpunk ethos more generally. What we want to achieve here is to provide people with all of the necessary resources that they need in order to provide services that they rely on for themselves and grow their autonomy rather than their dependence whenever they're interacting with technology.

Another big part of this is the ongoing push towards decentralization of social media networks and a growing emphasis on protecting digital citizens privacy. We want to aid in this by promoting the capabilities for communities to provide services for themselves and thus increase the level of trust that they have in those services.

A good example of this is the various above mentioned services geared towards replacing common social media accounts, within the context of federated networks. In the best of all worlds this would enable you as a social media user to have personal interactions with whoever admins your instance, which should increase the trust you have in that your privacy being protected.