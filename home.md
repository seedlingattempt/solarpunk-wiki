---
title: Welcome
description: Welcome to the Solarpunk Anarchism wiki! This is a place for all things that has to do with practically reorienting the development of your environment towards a solarpunk future. Feel free to contribute!
published: true
date: 2022-05-31T12:35:57.010Z
tags: intro
editor: markdown
dateCreated: 2021-10-01T14:54:36.227Z
---

# Welcome to the Solarpunk anarchism wiki!

This is a place intended for information gathering and dissemination geared towards Solarpunk praxis. What we mean by that is anything that concretely has to do with making *practical changes in the world that moves us towards a Solarpunk future.*

This is a pretty lofty goal and we don't think that it is one that will come easily. It will require a tremendous amount of work and effort on our behalf to go against the grain and do things the hard, but right, way. For instance this site is self-hosted, which is one of the first goals of this entire project. We live in a world of extreme interconnectedness, to the point where linchpins in the global communication system cannot be identified until they breakdown and take half the internet with them.

Our solution to this is to try to get more people up to speed when it comes to understanding the entirety of the information technology that allows them to live their lives and that includes ensuring that basic services that rely on the internet remain online despite being cut of hosting platforms in other countries or continents. Today this is not the case, but we hope we can improve the situation somewhat by giving people the knowledge they need to overcome what otherwise would be insurmountable obstacles.

This place is very much still under construction but we hope you'll join us in preparing all of us with the resilience we need to see us through that which is on the horizon. For more information on how to become part of the collective working on this site, see the [How to Contribute](/about/contributing) page.

## Overview of the content on this site 
### Tabs {.tabset}

#### Gardening

Gardening is the absolute first step in any autonomous and local food-production, regardless of climate or diet. Learning about how to take care for things 

- [Agriculture *A good starting place for our gardening and botany pages*](/agriculture)
- [Permaculture *Top page of our Permaculture section*](/agriculture/permaculture)
- [Guerrilla Gardening *Top page for our Guerilla Gardening section*](/agriculture/guerilla-gardening)
{.links-list}

#### Political Organizing
As anarchists, we have a vested interest in bringing power back down to the lowest levels of our society. If we are to make it through the coming decades and survive, we are going to need autonomous communities that can understaind their own needs and see that they are being met. This involves not only teaching ourselves about food production and technology, but also about the organizing methods and systems *themselves*.

These pages should help you get started with what you need to know in order to
- [ ] Organizing main page
- [ ] Run community assemblies link
- [ ] Doing Grassroots organizing
- [ ] Building mutual aid networks

#### Technology
Like it or not, 21st century technology is going to be a major part of life going foward regardless what forms society morphs into in the next couple of centuries. This makes it of the utmost importance that *everyone* has a bare minimum of technical litterary and we want to help define what that baseline ought to be while simultaneously providing resources for people who want to go deeper. 

- [Phone Security *Or how to keep control over your devices*](/technology/phone-security)
- [Self-hosting *Starting page for the self-hosting sections*](/technology/self-hosting)
{.links-list}


#### Right to Repair
Right to repair is going to be one of the major battlegrounds over the next decades that can swing both ways in terms of making the tech sector more or less ecologically friendly. This mainly has to do with hardware manufacturers finding themselves no longer getting regular performance gains that makes new devices marketable inherently. This is due to increasingly pushing up against the physical limitations of component manufacturing and we don't have cheap computing ready to go for general consumers beyond these limits, a new paradigm for computing would also take too long for the industry to get up and running, which means that we're going to be using the chips we have today for at least the next decade or so.

So the question becomes: How do we make the devices we already have, and which won't be massively outpaced performance-wise by future releases and updates, last us through the 2020s and well into 2030?