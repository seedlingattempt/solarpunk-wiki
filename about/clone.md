---
title: How to Clone
description: Short guide for how to clone the entirety of this project and ensure that you can keep a version of it in the face of our service suddenly failing
published: true
date: 2021-12-14T18:35:00.513Z
tags: 
editor: markdown
dateCreated: 2021-11-17T10:38:01.019Z
---

# How to Clone this project

This is a community run project that is being hosted out of closets and basements. As such there is a high probability that at some time in the future, this domain and server might one day cease to exist. The reasons for why this risk exists are not very interesting for this page, as this is mainly about what we can do about it.

Wikijs - which is powering this project - stores an up-to-date backup of all the pages and their history up to a year in a [git repository](https://codeberg.org/seedlingattempt/solarpunk-wiki). If you want to keep a copy of this project locally I would recommend mirroring that repository as a primary mode of syncing with this server.

If you are interested in doing that but don't know *how* up then please reach out to the administrators over at [The People Responsible](/about/people). That said, if you have suggestions for how a *better* system to do this would look, then please also get in touch and let us know!

Git is the primary mode of both backups and decentralization in place of a fully functioning federated wiki software.