---
title: Phone Security
description: A guide to increase phone security through security tools, alternate apps, and some not-so-common sense. 
published: true
date: 2022-01-08T05:03:39.501Z
tags: 
editor: markdown
dateCreated: 2021-12-31T11:57:26.726Z
---

Your phone is one of the most powerful surveillance devices ever created. It has location tracking, audio/video recording, proximity sensing with radios (bluetooth, data, etc) and the ability to transmit this data in the blink of an eye to governments, corporations, hackers, or anyone else who really wants your data. This page is meant to act as a resource for both tips and tutorials on how to protect your phone from the surveilance institutions of the world. 

# General Guidelines
Keep in mind that the only way to be 100% sure something won't be stolen from your phone is to not put it there. Anything that MUST be hidden from law enforcement/hackers/corporations should be done offline far away from any phones. 

## Use an Alternate Device
One of the most effective tactics for ensuring your activism identity is separate from your government identity. Use a second phone/computer, pay for it with cash and don't do anything that could link it to you/your other machines. Logging into your bank account, google or social media, or even into a video game profile that you've logged into on your main device can link your identity with that phone and compromise security. 

## Be Cautious on Social Media
Anything on social media should be considered public. Social media companies make profit by selling your data to advertisers & governments. Your posts, direct messages, and drafts are all stored on their server and subject to their privacy policy which usually involves selling your data to advertisers and freely sharing it with law enforcement. 
Feel free to continue using these platforms for flyering and propaganda because they're by far the widest reaching options for online communication, but do NOT do any action planning or illegal activity on these services, even in the DM's. Remember that your personal/legal/device identity is known by Facebook or similar, and don't send anything you wouldn't want read back to you in court. 

## Free and Open Source Softare (FOSS)
The FOSS movement existed before Solarpunk, but their values line up closely. All projects are free to download and use, work is often done by volunteers in the interest of creating tools that anyone can use, and all code run on the software is available for anyone to access/edit/review. Some offshoots of this (Copyleft, Public domain software, etc) are more strongly ideologically driven and closer to solarpunk ideals, but for security we'll be talking about an aspect shared by all: Open Source, which means that the software's code is pubicly available. 
When you download a closed source app, you can't really tell what it's doing in the background. That simple calculator app might be sending your location info to its servers, recording your audio/video, even mining crypto in the background, you can't tell without monitoring network and device activity very closely. With open source software, people are able to read through the source code of an app to be sure it's not doing anything you don't want. 
F-droid is a great repository for open source apps and should be your go-to for apps if you're on an android device. There's even a section of each app stating its privacy policy at a glance, saying "This app includes features you may not like" which you can "see more" to check out what exactly it may share. 




# How to de-Google your Android
One of the worst offenders of privacy rights. Google’s entire business model relies on collecting & selling data, primarily to advertisers. Their services arent "free", theyre paid for with your data. Location, device info, internet activity, you name it, they sell it to any corporation or government who is willing to buy information about you. 
This section is meant to be a guide to stopping google surveilance on your phone, both for a locked down activist device and for your everyday personal one. 



## Google Serives alternatives {.tabset}

### Chrome Web Browser

- Bromite - Based on the FOSS Chromium project (what Google Chrome is based on) with ad blocking and built in security hardening. Great option for a mobile browser, this handles webpages in a very similar way to the Chrome browser but without the unsolicited requests to Google's servers. 
- Firefox - The most widely-supported alternative, but not actually private unless you install a variety of extensions. (uBlock Origin, Decentraleyes, uMatrix, etc)
- Tor - based on firefox, uses onion routing (your traffic is downloaded by someone else, who encrypts it and sends it to someone else, who sends it to someone else, ..., who sends it to you to de-encrypt and view) This is perhaps the best option for hiding your IP address from websites you visit, but can be difficult to use regarding onion links and similar. 
- GrapheneOS and other security minded operating systems usually have their own default web browser based on Chromium with security-hardened features. This does require using the GrapheneOS operating system, see the "Android Operating System" section below.

Read more about web browsers: https://www.privacytools.io/#browser


### Youtube
- NewPipe - Free & Open Source Software, (FOSS) comes with all the features of youtube premium and has better privacy policy (anonymized data logging, only half your IP is stored)
- Peertube

### Google Play App Store
- F-droid is an app store filled to the brim with 100% FOSS apps. This is a great resource for apps and should absolutely be your go-to source if you need a new tool or feature on your phone, just don't expect to find your favorite brand or . 
- Aurora Store: Allows you to anonymously download apps that are on the Google Play Store, by receiving the file from Aurora's servers and not Google's. Aurora does not track the apps you install, but does give you the option to log into your google account to purchase apps. 
- Download apps in .apk format through your web browser: Some apps are only available by downloading directly from their website. Others might be paid apps that you don't want to use your credit card for. Downloading apps directly from their developers in .apk format will allow you to install them as if you'd just visited it on the play store. 

### Google Maps
- OpenStreetMaps which instead of being compiled & updated by googlecorp, it's updated by us, the users. 

- OSMand+ has live navigation with turn by turn directions you're used to seeing in google maps
- OSMedit allows you to make edits to the map. Local coffee shop got replaced by a starbucks? Go ahead & update it to let everybody else know*


### Gmail 
- Stop using your governmentname@Gmail.com address. Google’s privacy policy allows them to read full content of all your emails. 
- Use an alternate eMail address. use your activist name for this address and do NOT use that same address for shit like paypal or anything that could link your government name to your activities. 
- Keep in mind that your email is only as secure as where you send it. Don’t send spicy emails to a Gmail address, then google received a copy of your content AND your Alt email is now linked to their gmail account.

Private email alternatives:
- ProtonMail - Both free & paid options, actual end to end encryption and options for increased privacy through steeper encryption and PGP keys.
- Tutanova - Essentially same as Proton, just a different company. 
- RiseUp - Made by and for activists, this is the best option in my book. Invite only, I don't have one. 

### Google Drive (Cloud Storage)
- Proton Drive - Hosted by protonmail, same privacy policy of end to end encryption when shared with other proton users, allows keys or passwords to be created and sent separately or together. Paid plans offer more storage. 
- Mega.co.nz? Created by Kim Dotcom after the end of Megaupload, intended to be a private cloud storage platform with encryption where Mega does not hold the keys. [Investigate the privacy policy pls i have not recently]
- Riseup has a drive right? Either way it's invite only

## Android Operating system (Hardest yet most effective)
Android is a loose name for the family of default operating systems on most phones on the market. There are many different versions of Android, usually the one installed on a stock phone is a created by your phone’s manufacturer in collaboration with Google. All of these may or may not contain tracking info, as they are closed source. They all are based on the FOSS android project. 
    -GrapheneOS is a security-hardened version of the open source android project, which has NO google play services which does limit the effectiveness of your phone (cant use any apps that use GSF services, for example Solar Surveyor will not launch, or the maps on doordash don't work but the app itself still works, other apps wont even launch)



# Resources
https://cldc.org/security-updates Civil Liberties Defence Center is a group of lawyers in Eugene Oregon USA which "supports movements that seek to dismantle the political and economic structures at the root of social inequality and environmental destruction." As such they frequently post security guides & articles on their website. Here's their 2021 reccomendations list: https://cldc.org/digital-security-recommendations-for-2021/
https://www.reddit.com/r/degoogle/ - Community of folks dedicated to getting off Google services, the sidebar has a great list of alternatives & guides. 
https://privacyguides.org/ [saw this linked elsewhere but haven't used it myself]
https://www.youtube.com/c/TheHatedOne 