---
title: Visions of Onomaturgy
description: To imagine a better future, we need new concepts. 
published: true
date: 2022-01-06T11:13:11.517Z
tags: creative
editor: markdown
dateCreated: 2021-12-10T07:58:00.685Z
---

# Purpose
Solarpunk aims not only at changing the world for the better, but also at telling new, refreshing and hopeful stories. To do so, we need the ability to tell those stories in a new way, detached from the widespread frameworks of punishment, hierarchy and profit. Here are some tentative concepts, both coming from our crew and other modern thinkers that we identify as solarpunk.

*"A word is a model of reality as we think it."* - Ludwing Wittgenstein

# Suggested Words
Here follows some summaries and some in-progress naming of concepts which we think will be useful in talking about the future world and actions to take there

### Civirtual/Civirtualization
[From "civil" and "virtual"] A group of consensual netizens cohabiting a digital space on the internet, usually for mutual support or shared worldview.

### Convivialism
Doctrine based on sharing and prioritizing each other's wellbeing as a primary daily activity. Involves the minimization (or the outright refusal) of economic transactions between individuals.

### Capacitage
Portmanteau of "Capacity" and "Sabotage". The noun and verb means to increase the amount of work to be done to such an extent that the capacity to do so no longer exists. A common example would be the practice of lawyers sending trucks full of junk to their rivals during discovery in the hopes that they will drown in the signal-to-noise ratio thus introduced.

### Solutionary
Portmanteau of "Solution" and "Revolutionary". A person with radical views that is not focused on destroying and dismantling but on proposing solutions to problems as its core modus operandi. Solarpunk is inherently solutionary in spirit.

### Supermanence/onliving
The human wish for something inherently transient in nature to last well beyond it's actual use or coherence would have allowed without human intervention. Opposite of "planned obsolescence". Examples include the wish to preserve late 19th / early 20th century housing in European inner cities for aesthetic reasons even though they are no longer functional for a 21st century modern life.

### Eventization
The wish to condense a drawn-out process of change into single events of change when making metaphors. When discussing climate change one big thing that people bring up is "The Greenland glaciers slipping into the Gulf Stream and pulling the temperatures way down in the norhtern atlantic hemisphere". How long time this "Slipping" will take is always left unsaid however, and the truth is that we most likely talk about it right now becuase *it is already happening*. There will be no *single day* in which an iceblock the size of a country will slip into the Atlantic and stop the stream dead, but somehow we're still expecting it to.

Basically, when talking about nature, it refuses to behave like humans do. We have points of decision and change which are much more easily pinpointed (but are still often emphasized to much a higher extent than what is actually reasonable): declarations of war, asssasinations of individual people sparking change, specific momentary boiling points. Nature does not work this way, and even climatic tipping points will take years if not decades to make themselves known even though while they are in the process of "tipping", from the perspective of an individual every step of the way will be felt as the new "normal" without any single point where The Event™ has definitely happened.

We are not saying that these events *cannot* happen, we're just saying that these events are just momentary blips of change in an overall *process* of change which dwarfs any individual event.

For examples, see:
- *Don't Look Up* Netflix Movie --- in the movie, climate change is taking the metaphorical shape of a comet hurling towards earth and no effects of its existance can be felt before impact
- *Mars Trilogy* Kim Stanley Robinsson --- Climate change is summed up as a single event in which large parts of Antarctic ice-sheets break of and drop into the Indian Ocean, after which follows weeks of unspeakable destruction in coastal cities due to the water-level suddenly rising several meters in a very short period of time.

# Pre-existing Concept Words

### Ecocide

Substantially damaging or destroying ecosystems or by harming the health and well-being of a living species, including humans. Currently not (yet) codified as a punishable crime by international law.

- [ ] Need a word for a person who performs it like: homicide -> Murderer

### Egoizing
Frequently occuring in *The Dispossessed* by Urusla K Le Guin, "Egoizing" is a catch-all for behavior whose primary function is for the speaker to separate from a collective and mark themselves as distinguished in some way, setting the precident for more concrete hierarchies based on distinguishing between people.

### Grok
As conceptualized by Robert A. Heinlein in his novel *Stranger in a Strange Land*, "to grok" is to intimately understand something through the process of empathy, blurring the lines between subject and object. The concept is nuanced and this short definition should be subject to editorial review.

