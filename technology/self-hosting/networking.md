---
title: Networking
description: Top level page for networking with a center on self-hosting.
published: true
date: 2021-12-31T12:15:43.980Z
tags: self-hosting, networking, technology
editor: markdown
dateCreated: 2021-11-19T17:34:08.628Z
---

# Networking

Networking is a big subject, luckly we are not going to have to cover the entirey of it on this page. What we are mainly interested in how networking is fundamental to letting us set up our own services for our own needs, while still being easy to use for people that are less technically inclined than us. If this is your first time messing around with router configurations or buying domain names, don't worry, nothing you can do here will break the pentagon and most of it is usually very straightforward operations.

## The Internet, a quick summary

Thinking about the internet is difficult, but we're going to give it a go

- [ ] Add this secton

For more on the intricacies of the internet we can suggest [James Balls](https://en.wikipedia.org/wiki/James_Ball_(journalist)) *The System*. It is a good introduction for the uninitiated to how the internet came to be the beast that it is today and why it is so hard to get people to agree on what purposes it should serve and how it should be governed.

## What is internet speed

For the purposes of this section we're going to be using the example of a peertube instance, just to get a feel of the general size of data that is required to pass from your instance and into the internet.


### Download

### Upload

### Latency

## ISPs

Your ISP (internet service provider) is the first entity that you would encounter if you were to follow your internet connection from your router and out the door of your home. Depending on where you live and the way that your contract looks they might even own the router in your home.

For our purposes they are interesting because they are going to be the ones with the most vested interest in how you utilize your internet connection. In modern times ISPs have battled hard against allowing Netflix and YouTube free access to their customers, arguing that because of the massive dataflows that these companies rely on, they should have to pay more for using the cables that they don't own themselves.

The ISPs in your area might be equally pissed off once they realize that you are serving services from your home without using various "company subscriptions" and paying a lot more for your internet, despite the fact that you're just using the speeds you are paying for. Some ISPs go so far as to sneakily disable the capabilities for you as a private person, to allow certain levels of security for your potential users through blocking specific ports from being utilized. The same ports that we would want to use for serving websites and services to our users would thus be blocker off, without us ever having been informed about it. ISPs are also notoriously tight-lipped about this, seeing as people who take an active interest in utilizing all aspects of their uplink to their internet (as we are now trying to do), will naturally be more difficult and costly customers for them than the avarage person.

So when you start messing around with routers in the following pages of these guides, remember that sometimes the fault can lie outside of your control, which you will then have to sidestep in order to setup the services that you need. We have some suggestions for how this can be done.

## Next steps

- [Domains *For setting up human readable names for your services*](/technology/dns)
- [Routers *For making sure that your services are accessible*](/technology/routing)
- [Encryption *For making sure that your services are secure*](/technology/encryption)
{.links-list}