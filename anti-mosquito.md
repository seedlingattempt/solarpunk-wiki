---
title: Getting rid of Mosquitoes
description: 
published: true
date: 2022-06-22T16:25:30.501Z
tags: mosquito, pest, pesticide
editor: markdown
dateCreated: 2022-06-16T05:53:31.068Z
---

# Getting Rid of Mosquitoes
There are a variety of ways to deal with mosquitoes
<br>
1. **Eliminate standing water**
-Mosquitoes lay their eggs on stagnant bodies of water.
-This becomes a problem especially outdoors in the rainy season. Where empty petbowls, empty pots, saucers, buckets, and the like, accumulate shallow stagnant water. Make sure to dry them and turn them over so they don't catch water.
1. **Populate with fish**
-Otherwise if the pool of water is big enough, you may want to consider populating it with fish. Any variety of fish will eat the mosquito larvae. They don't have to be too large either, you can have small fishes like platys, guppies, and tetras. 
1. **Grow mosquito repellant plants**
-These are a valuable option especially as companion plants in gardening, Examples of which are clove, eucalyptus, lavender, marigold, lemon grass, basil.
Marigold especially is a popular option for gardeners, usually had as an ornamental border in vegetable patches. It contains pyrethrum which is malodrous to mosquitoes, as well as other insects.
<br>
<br>
### Resources:
<br>
<br>
Mosquito repellant plants:

https://mosquitoreviews.com/mosquito-repellents/plants

https://mosquitoreviews.com/mosquito-repellents/basil/

https://mosquitoreviews.com/mosquito-repellents/marigold/

https://www.kellogggarden.com/blog/gardening/marigold-how-to-care-for-and-grow-companion-plant-and-more/
<br>
<br>
Small Fishes:

https://www.thesprucepets.com/small-aquarium-fish-breeds-for-freshwater-5120495 
<br>
<br>
Controlling Mosquito Population(2019)Discover Permaculture with Geoff Lawton
https://www.youtube.com/watch?v=WOeTdUqAOWY


Dealing with Mosquitoes and Reading the Landscape | Permaculture Q&A(2020)Discover Permaculture with Geoff Lawton
https://www.youtube.com/watch?v=60ZTIJi6tbI&t=418s