---
title: Bokashi Composting, Troubleshooting & Things to Remember
description: Things to keep in mind and watch out for when Bokashi Composting
published: true
date: 2022-06-22T16:16:17.405Z
tags: compost, home-gardening, bokashi
editor: markdown
dateCreated: 2022-06-18T08:47:51.166Z
---

# Troubleshooting your Bokashi

### When has a Bokashi Compost Failed?

MOULD
	The presence of white mould is fine, however the presence of black/blue/green mould, means that the fermenting process has failed.
	If the bad black/blue/green mould appears then the bokashi should be discarded.    

SMELL
	It should smell sweetish, sour, cheesy, pickly. It should not smell foul and rancid. If it smells foul then the bokashi should be discarded.

### Are flies and maggots bad?
    
FLIES/MAGGOTS
	If you only have flies and maggots in your bokashi, this should not be a cause of concern. In fact, they'll add a little protein to the compost. To deal with them you can add more of the EM solution to make the compost more acidic, and hopefully kill the flies. Make sure it doesn't get too wet though as this will be inviting to maggots. You should also check to make sure the lid is on tight, without much oxygen they will die.


<br>

### COMMON PROBLEMS:

**1. SIZE**
    Don't put things too big into the bokashi, make sure to chop them up first. A rule of thumb is not to put anything thicker or longer than your thumb. The larger it is the longer it will take to ferment.



**2. AIR**
    Make sure to keep it compressed and compact, when you add the layers of food scrap. Bokashi is essentially fermentation, an anaerobic process. You want to make sure there's not too much air mixed in.
    Make sure the lid was kept tightly closed. Again you don't want too much air flow, due to the anaerobic process.
This might also cause flies to enter and infest your bokashi with maggots.

**3. MICROBES**
   	Be generous with spraying the EM-1 solution, you can't use too much.
	Don't put things that are already getting rotten and mouldy(it will compete with the microogranisms of the bokashi).


**4. TEMPERATURE**
    Make sure the bin is placed in an area around room temperature, in a shaded place.

**5. MOISTURE**   
   	Make sure it's not too moist. Either the drain is not working, or you need to add more drying medium. It shouldn't be too wet such that things are drenched, sloppy and soggy. 	Being too wet will cause it to be more inviting to maggots.
	Too much moisture will also cause it to be smelly. Being smelly won't affect the end result, it can be however be inconvenient for those doing bokashi indoors.
	To see if too much moisture is the problem check the underside of the lid. If there is condensation, this is a sign to add drying mediums. These can be shredded newspaper, an egg carton or something absorbent like old bread. 
	Neither should it be too dry, microbes need a moderate amount of moisture roughly the same as good soil, otherwise nothing will happen.

**6. TIME**
	The bokashi coposting bucket should ideally be opened only once or twice a day, as your filling it up with food scraps. Even better if only every other day. Collect first the food scraps for the day in a different container, rather constantly opening and closing the composting bucket throughout the day as you add food scraps.
	Once the composting bucket has been filled with food scraps, two weeks is a good rule of thumb for the fermenting process, there's no problem with leaving it for longer. But it might take longer if the food scraps are not chopped well enough. If it takes several weeks to fill the composting bucket, one week of fermentation might be sufficient.










**Sources:**
1. Bokashi Composting from Start to Finish (DIY Bokashi Bucket). (2019). *Bokashi Composting from Start to Finish (DIY Bokashi Bucket)*. Youtube. https://www.youtube.com/watch?v=0k3PTUnDHSI&t=491s
1. Bokashi Living. (2017). *Troubleshooting: What to do if your bokashi bin goes bad*.
https://bokashiliving.com/troubleshooting-what-to-do-if-you-bokashi-bin-goes-bad/
1. Turning to Green. (n.d.). *Bokashi Troubleshooting*.
https://www.turningtogreen.com/post/bokashi-troubleshooting
1. Bokashi World. (n.d.). *FAQ*
https://bokashiworld.blog/learn/faq/
1. Compost Revolution. (n.d.). *I have bugs maggots flies in my bokashi bin what do I do*
https://help.compostrevolution.com.au/en/articles/3757839-i-have-bugs-maggots-flies-in-my-bokashi-bin-what-do-i-do
1. Epic Gardening. (n.d.). *Maggots in Compost*
https://www.epicgardening.com/maggots-in-compost/