---
title: Bokashi Composting
description: For composting your kitchen scraps, in small spaces indoors.
published: false
date: 2022-06-22T16:19:11.127Z
tags: compost, home-gardening, bokashi
editor: markdown
dateCreated: 2022-05-20T15:55:44.435Z
---

# *W-I-P* Bokashi Composting 

Should this be separate stuff that are linked into here Bokashi Composting?

Is EM(Effective Microorganism) also known as LABS(Lactic Acid Bacteria Solution)?

## How to do Bokashi Composting
-you need EM solution
-compost bucket
-drying medium


1.essentially chuck in your food scraps(make sure they're not rotten) in the  bucket
2.spray with the EM
3.if it gets to moist, add the drying medium or soil


<br>
<br>

## EM Solution

Ingredients:
1. Rice, *1 cup*
2. Milk, *1 liter* 
3. Water *~30 liters*
4. Sugar(Brown or Molasses) *360mL*
5. liquid container/s(jar, bottle, etc.) *1 liter capacity* 


Procedure:
### 1.Wash some Rice.
Pour water into the rice. Around 1:1 rice-to-water ratio.
*Make sure water is not chlorinated, as some tap water are.*

Rinse the rice in the water till the water turns foggy, like thin milk.
![rice-water_wash-min.png](/rice-water_wash-min.png)
Source: *Permaculture Retreat Center* [^1]
<br>
<br>
### 2.Pour Rice-Water into a Container
Strain out the rice from the now foggy rice water.
And store that foggy rice-water in a jar, or plastic bottle, etc.
<br>
<br>
### 3. Cover the Container
You can either cover the container with a paper napkin, cheese cloth or other breathable cloth/towel. And held using a rubberband.

![rice-water_cover-min.png](/rice-water_cover-min.png)
Source: *Fraser Valley Rose Farm* [^2]

Alternatively, you can just lightly screw on the cover. Keeping it **not tightly** sealed allows gases to escape as it ferments. 
![rice-water_bottle-min.png](/rice-water_bottle-min.png)
Source: *Permaculture Retreat Center* ^1^
<br>
<br>
### 4.Leave Rice-Water to Ferment
Store in a cool, shaded, dry place until fermentation. You can tell it's fermented by the formation of a film at the top of the liquid, as well as a slightly sour smell like cheese or sourkraut.

![rice-water_ferment.png](/rice-water_ferment.png)
Source: *Fraser Valley Rose Farm*^2^
<br>
Most recommend that this will take about a week.
In the *Sydney Tay*^3^ video this only took three days.
<br>
<br>
### 5.Mix Rice-Water with Milk
Take the ricewater's middle section(where the microorganisms are) and mix with the milk. 
The milk-to-ricewater ratio is 10:1. So if you only have a 1 liter of milk, you'll only need to take 100mL of the ricewater.
<br>
*Fray*[^2] uses a rubber pipette, and *Sydney*[^3] uses a metal straw, to specifically get the middle liquid. But *Auxhart*[^6] and *Permaculture Retreat*[^1] just pour and mix w/o being too particular about it.

Optionally *Permaculture Retreat*[^1] adds sugar/mollasses to the ricewater-milk mix at a ratio of 100:3. That is one hundred ricewater-milk mix for every three sugar.
<br>
<br>
### 6.Leave Ricewater-Milk Mix to Ferment
Once again **cover lightly**, to let gases escape. And store in a cool, shaded, dry place until it ferments.
This time it's indicated by seeing curds at the top and whey down below. The liquid itself will have turned yellowish.

![em-1_solution.png](/em-1_solution.png)
Source: *Auxhart Gardening*[^6]
<br>
<br>
### 7. Mix with Water and Sugar
Mix water, sugar, and ricewater-milk mix at a ratio of 94:3:3 respectively.
This is the final EM-1 solution.

That is to say 1 liter of ricewater-milk mix can be used to make about 30 liters of EM-1 solutions.  
<br><br>
You can also use whey from yogurt(*Carazy*^4^)
<br>
**See sources for further elaborations.**

*Permaculture Retreat* ^1^ shows a really simple, down to earth procedure and materials, that make you say "oh wow that looks easy" compared to the other's using fancy flasks and pipettes.

*Fraser* ^2^ and *Sydney* ^3^ do a good job of explaining nuances like indicators of what you want to see for successful fermentation.
<br>
Keeping it alive with further solutions
<br>
<br>

## Compost Bucket
Materials:
1. 2 Buckets
2. Newspaper/Manila Paper/ Cloth/ Plant Netting



### 1. Drill small holes onto bottom of first bucket
![bokashi_drillhole.png](/bokashi_drillhole.png)
Source: *Sidney Tay*
<br>

### 2. Stack the first bucket onto the second bucket
![bucket_stack_diagram.png](/bucket_stack_diagram.png)

You want there to be an air gap at the bottom, where the leachate from the compost can drain to, and be periodically disposed.
If you're buckets fit too well together and don't leave an air gap; you can place a brick to prop the first bucket and create that air gap at the bottom.

<br>

## 3. Seal the Bucket 
Seal this like one would other fermentation processes. 
I've seen three ways that this has been done.
1. Wrap
with newspaper/manila paper/ breathable cloth
use rope to tie the wrap tightly/large rubber band/simply wrap tightly like a gift wrapper.
![bokashi_seal2-min.jpg](/bokashi_seal2-min.jpg)
Source: *Bokashi Pinoy* [^10]
<br>

2. Lid
Simply use you're bucket's lid(if it has one)
![bokashi_bucketlid-min.png](/bokashi_bucketlid-min.png)
Source: *Epic Gardening* [^5]

Or you can take any cover(plywood, bin cover etc.) and weigh it down(see next image).
<br>

3. Wrap + Lid
![bokashi_seal-min.png](/bokashi_seal-min.png)
Source: *Bokashi Pinoy* [^10]
<br>


<br>
<br>

## Drying Medium 
Traditionally people use bran, but you can use any available organic dry medium. 
*Permaculture Retreat Center*^1^ just sprays the EM-1 solution directly to food scraps without using a medium though.
Its a moisture absorbant to help reduce smell and avoid it from going bad (*Sydney Tay*^3^).

e.g.
1. Newspaper (and other paper/carton scraps)
2. Sawdust
3. Coffee Grounds
4. Bran (Rice, Wheat, etc.)
_________________
Sources:
[^1]: DIY EM-1 Solution/Bokashi and its Uses (*Permaculture Retreat Center*):
https://www.youtube.com/watch?v=cgAbpXescAc
[^2]: DIY EM-1 Solution/Bokashi(*Fraser Valley Rose Farm*):
https://www.youtube.com/watch?v=sUEVu32rcyQ
[^3]: DIY EM-1 Solution/Bokashi(*Sydney Tay*):
https://www.youtube.com/watch?v=lcTEw5uchuQ
[^4]: DIY EM-1 Solution/Bokashi(*Carazy*):
https://carazy.net/diy-home-made-free-em-bokashi-mix/
[^5]: DIY Bokashi Bucket (*Epic Gardening*):
https://www.youtube.com/watch?v=0k3PTUnDHSI
[^6]: DIY Bokashi Bucket (*Sydney Tay*):
https://www.youtube.com/watch?v=SXnULh6AMxI
[^7]: Demonstration of using Newspaper Medium (*Auxhart Gardening*): 
https://www.youtube.com/watch?v=mE8uCCfeO8U
[^8]: Bokashi Bran Substitutes(*Reddit*):
https://www.reddit.com/r/bokashi/comments/oq4f5n/bokashi_bran_substitute/
[^9]: Reddit Thread on Bokashi Bran Using Sawdust(*Reddit*):
https://www.reddit.com/r/bokashi/comments/spi6z9/bokasi_bran_using_sawdust/
[^10]: Bokashi Pinoy:
https://www.bokashipinoy.com/post/myth-buster-air-tight-bokashi-buckets