---
title: Hardware
description: A beginners overview to the hardware used for self-hosting in particular and broader IT usage in general.
published: true
date: 2021-12-31T12:16:16.092Z
tags: 
editor: markdown
dateCreated: 2021-12-30T12:31:59.037Z
---

# Hardware
A non-comprehensive beginners guide to understanding what the different parts of 21st century computer does and how they function as a whole, with particular interest towards networking.

## What this page isn't



## Clients and Servers and the rest

### Server

### Client

### The Rest

#### Cables

#### Wi-Fi


## Terminology
- Gateway
- Router
- Switch
- Server
- Client


## Laptops vs Desktops vs Servers, a quick summary