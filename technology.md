---
title: Technology
description: Index page for Solarpunk technology practices
published: true
date: 2022-01-08T10:40:17.406Z
tags: 
editor: markdown
dateCreated: 2021-12-31T12:11:36.290Z
---

# Technology

- [Phone Security](/technology/phone-security)
- [Self-hosting](/technology/self-hosting)
{.links-list}